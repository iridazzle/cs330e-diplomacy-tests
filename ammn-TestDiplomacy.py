#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    def test_solve_1(self):
        r = StringIO("Z Austin Hold\nA Houston Hold\nR Boston Move Austin\nD Dallas Support Z")
        w = StringIO()
        resulting_list = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Houston\nD Dallas\nR [dead]\nZ Austin\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nT Miami Support B\nF Seattle Support B")
        w = StringIO()
        resulting_list = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Austin\nF Seattle\nT Miami\n")

    
    def test_solve_3(self):
        r = StringIO("A Seattle Move Detroit\nB Detroit Move Seattle\nC Denver Support B\nD Austin Move Denver")
        w = StringIO()
        resulting_list = diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Detroit\nB Seattle\nC [dead]\nD [dead]\n")

# ----
# main
# ----
if __name__ == "__main__": #pragma: no cover
    main()
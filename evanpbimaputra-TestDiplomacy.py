from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from io import StringIO

class TestDiplomacy (TestCase):

    def test_diplomacy_1(self):
        r = StringIO("A Casablanca Hold\nB Marrakesh Hold\nC Rabat Hold\nD Fes Move Rabat")
        w = StringIO()
        output = "A Casablanca\nB Marrakesh\nC [dead]\nD [dead]\n"
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), output)

    def test_diplomacy_2(self):
        r = StringIO("A Kyiv Support C\nB Luhansk Support C\nC Mariupol Hold\nD Kharkiv Move Mariupol")
        w = StringIO()
        output = "A Kyiv\nB Luhansk\nC Mariupol\nD [dead]\n"
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), output)
    
    def test_diplomacy_3(self):
        r = StringIO("A Jakarta Move Medan\nB Medan Move Bandung\nC Bandung Move Surabaya\nD Surabaya Move Jakarta")
        w = StringIO()
        output = "A Medan\nB Bandung\nC Surabaya\nD Jakarta\n"
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), output)

    def test_diplomacy_4(self):
        r = StringIO("A Ottawa Move Vancouver\nB Vancouver Support C\nC Toronto Hold\nD Edmonton Hold")
        w = StringIO()
        output = "A [dead]\nB [dead]\nC Toronto\nD Edmonton\n"
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), output)


if __name__ == "__main__":
    main()
